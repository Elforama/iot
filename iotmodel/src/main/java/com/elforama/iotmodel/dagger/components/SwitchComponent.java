package com.elforama.iotmodel.dagger.components;

import com.elforama.iotmodel.IoTProvider;
import com.elforama.iotmodel.dagger.modules.NetworkingModule;
import com.elforama.iotmodel.dagger.modules.SwitchModule;
import com.elforama.iotmodel.switches.SwitchModelDummyImpl;
import com.elforama.iotmodel.switches.SwitchModelImpl;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by jonathanmuller on 10/15/16.
 */

@Singleton
@Component(modules = {NetworkingModule.class, SwitchModule.class})
public interface SwitchComponent {
    void inject(SwitchModelImpl switchModel);
    void inject(SwitchModelDummyImpl switchModel);
    void inject(IoTProvider iotProvider);
}

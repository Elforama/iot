package com.elforama.iotmodel.dagger.modules;

import com.elforama.iotmodel.switches.SwitchModel;
import com.elforama.iotmodel.switches.SwitchModelDummyImpl;
import com.elforama.iotmodel.switches.SwitchModelImpl;
import com.elforama.iotmodel.switches.SwitchService;
import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;
import javax.inject.Named;
import javax.inject.Singleton;

/**
 * Created by jonathanmuller on 9/16/16.
 */
@Module
public class SwitchModule {

    @Provides
    @Singleton
    public SwitchService provideSwitchService (Retrofit retrofit) {
        return retrofit.create(SwitchService.class);
    }

    @Provides
    @Singleton
    @Named("real")
    public SwitchModel provideSwitchModel() {
        return new SwitchModelImpl();
    }

    @Provides
    @Singleton
    @Named("dummy")
    public SwitchModel provideSwitchModelDummy() {
        return new SwitchModelDummyImpl();
    }
}
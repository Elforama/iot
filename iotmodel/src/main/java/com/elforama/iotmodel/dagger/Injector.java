package com.elforama.iotmodel.dagger;

import com.elforama.iotmodel.dagger.components.DaggerSwitchComponent;
import com.elforama.iotmodel.dagger.components.SwitchComponent;
import com.elforama.iotmodel.dagger.modules.NetworkingModule;
import com.elforama.iotmodel.dagger.modules.SwitchModule;

/**
 * Created by jonathanmuller on 9/16/16.
 */
public class Injector {

    public SwitchComponent switchComponent;

    private static Injector instance;

    private Injector() {
        switchComponent = DaggerSwitchComponent
                .builder()
                .networkingModule(new NetworkingModule("http://jamtime.ddns.net:4001/"))
                .switchModule(new SwitchModule())
                .build();
    }

    public static Injector getInstance() {
        if (instance == null) {
            instance = new Injector();
        }
        return instance;
    }
}
package com.elforama.iotmodel.switches;

import rx.Observable;
import rx.functions.Func1;

import java.util.*;

/**
 * Created by jonathanmuller on 9/18/16.
 */
public class SwitchModelDummyImpl implements SwitchModel {

    SwitchInfo switch1 = new SwitchInfo(1, false, "Living room");
    SwitchInfo switch2 = new SwitchInfo(2, true, "Desk lamp");
    SwitchInfo switch3 = new SwitchInfo(3, true, "Dining room");
    SwitchInfo switch4 = new SwitchInfo(4, false, "Side lamp");
    SwitchInfo switch5 = new SwitchInfo(5, false, "Kitchen");
    SwitchInfo switch6 = new SwitchInfo(6, true, "Bedroom");

    Random rand = new Random();

    List<SwitchInfo> switches = new ArrayList();

    SwitchInfoList switchInfoList = new SwitchInfoList();

    public SwitchModelDummyImpl() {
        switches.add(switch1);
        switches.add(switch2);
        switches.add(switch3);
        switches.add(switch4);
        switches.add(switch5);
        switches.add(switch6);

        switchInfoList.devices = switches;
    }

    @Override
    public  Observable<SwitchInfo> fetchSwitch(final int id) {
        return Observable.from(switches)
        .filter(new Func1<SwitchInfo, Boolean>() {
            @Override
            public Boolean call(SwitchInfo switchInfo) {
                return switchInfo.id == id;
            }
        }).first();
    }

    @Override
    public Observable<List<SwitchInfo>> fetchSwitches() {
        return Observable.just(switches);
    }

    @Override
    public List<SwitchInfo> getSwitches() {
        return switches;
    }

    @Override
    public Observable<SwitchInfo> setSwitch(int id, boolean on) {
        return Observable.just(new SwitchInfo(id, on, "Fake Switch"));
    }

    @Override
    public Observable<SwitchInfo> createSwitch(String name) {
        SwitchInfo switchInfo = new SwitchInfo(rand.nextInt(), false, name);
        return Observable.just(switchInfo);
    }

}
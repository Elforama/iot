package com.elforama.iotmodel.switches;

import android.widget.Switch;

import com.elforama.iotmodel.dagger.Injector;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;
import rx.Scheduler;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

import javax.inject.Inject;

/**
 * Created by jonathanmuller on 9/16/16.
 */
public class SwitchModelImpl implements SwitchModel {

    @Inject
    SwitchService switchService;

    private List<SwitchInfo> switchInfoList = new ArrayList<>();

    public SwitchModelImpl() {
        Injector.getInstance().switchComponent.inject(this);
    }

    @Override
    public Observable<SwitchInfo> fetchSwitch(int id) {
        return switchService.getSwitch(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<List<SwitchInfo>> fetchSwitches() {
        return switchService.getSwitches()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext(new Action1<List<SwitchInfo>>() {
                    @Override
                    public void call(List<SwitchInfo> switches) {
                        switchInfoList = switches;
                    }
                });
    }

    @Override
    public List<SwitchInfo> getSwitches() {
        return switchInfoList;
    }

    @Override
    public Observable<SwitchInfo> setSwitch(int id, boolean on) {
        return switchService.setSwitch(id, on ? 1 : 0)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext(new Action1<SwitchInfo>() {
                    @Override
                    public void call(SwitchInfo switchInfo) {
                        for (int i = 0; i < switchInfoList.size(); i++) {
                            if (switchInfoList.get(i).id == switchInfo.id) {
                                switchInfoList.set(i, switchInfo);
                            }
                        }
                    }
                });
    }

    @Override
    public Observable<SwitchInfo> createSwitch(String name) {
        return switchService.createSwitch(new SwitchInfo(1, false, "test"))
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }
}
package com.elforama.iotmodel.switches;

import retrofit2.http.GET;
import retrofit2.http.Path;
import rx.Observable;
import java.util.*;

/**
 * Created by jonathanmuller on 9/16/16.
 */
public interface SwitchService {

    @GET("iot/api/v1.0/devices/{id}")
    Observable<SwitchInfo> getSwitch(@Path("id") int id);

    @GET("iot/api/v1.0/devices")
    Observable<List<SwitchInfo>> getSwitches();

    @GET("iot/api/v1.0/devices/{id}/{on}")
    Observable<SwitchInfo> setSwitch(@Path("id") int id, @Path("on") int on);

    Observable<SwitchInfo> createSwitch(SwitchInfo switchInfo);
}
package com.elforama.iotmodel.switches;

/**
 * Created by jonathanmuller on 9/16/16.
 */
public class SwitchInfo {

    public int id;
    public boolean isOn;
    public String name;

    public SwitchInfo(int id, boolean isOn, String name) {
        this.id = id;
        this.isOn = isOn;
        this.name = name;
    }
}
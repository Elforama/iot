package com.elforama.iotmodel.switches;

import rx.Observable;
import java.util.*;

/**
 * Created by jonathanmuller on 9/16/16.
 */
public interface SwitchModel {

    Observable<SwitchInfo> fetchSwitch(int id);

    Observable<List<SwitchInfo>> fetchSwitches();

    List<SwitchInfo> getSwitches();

    Observable<SwitchInfo> setSwitch(int id, boolean on);

    Observable<SwitchInfo> createSwitch(String name);
}
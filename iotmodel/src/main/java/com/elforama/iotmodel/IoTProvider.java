package com.elforama.iotmodel;


import com.elforama.iotmodel.dagger.Injector;
import com.elforama.iotmodel.switches.SwitchModel;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Created by jonathanmuller on 9/17/16.
 */
public class IoTProvider {

    @Inject
    @Named("real")
    public SwitchModel switchModel;

    private static IoTProvider instance;

    private IoTProvider() {
        Injector.getInstance().switchComponent.inject(this);
    }

    public static IoTProvider getInstance() {
        if (instance == null) {
            instance = new IoTProvider();
        }
        return instance;
    }
}
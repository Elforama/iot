package com.elforama.smartcontrol;

import android.util.Log;
import com.elforama.iotmodel.IoTProvider;
import com.elforama.iotmodel.switches.SwitchInfo;
import com.elforama.iotmodel.switches.SwitchModel;

import java.util.List;

import rx.Observer;

/**
 * Created by jonathanmuller on 9/18/16.
 */
public class MainScreenPresenter implements MainScreenMvp.Presenter{

    private static final String TAG = "MainScreenPresenter";

    private SwitchModel switchModel;
    private MainScreenMvp.View view;

    @Override
    public void onBindView(MainScreenMvp.View view) {
        this.view = view;
        switchModel = IoTProvider.getInstance().switchModel;
    }

    @Override
    public void onLoadData() {
        switchModel.fetchSwitches().subscribe(new Observer<List<SwitchInfo>>() {
            @Override
            public void onCompleted() {
                Log.d(TAG, "onCompleted: ");
            }

            @Override
            public void onError(Throwable e) {
                Log.d(TAG, "onError: " + e.getLocalizedMessage());
            }

            @Override
            public void onNext(List<SwitchInfo> switchInfoList) {
                Log.d(TAG, "onNext: ");
                view.onDataLoaded(switchInfoList);
            }
        });
    }

    @Override
    public void onSwitchChanged(int id, boolean isOn) {
        switchModel.setSwitch(id, isOn).subscribe(new Observer<SwitchInfo>() {
            @Override
            public void onCompleted() {
                view.onDataLoaded(switchModel.getSwitches());
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(SwitchInfo switchInfo) {

            }
        });
    }
}
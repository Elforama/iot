package com.elforama.smartcontrol;

import com.elforama.iotmodel.switches.SwitchInfo;
import java.util.*;

/**
 * Created by jonathanmuller on 9/18/16.
 */
interface MainScreenMvp {

    interface View {
        void onDataLoaded(List<SwitchInfo> switchInfoList);
        void onError(String error);
    }

    interface Presenter {
        void onBindView(View view);
        void onLoadData();
        void onSwitchChanged(int id, boolean isOn);
    }
}
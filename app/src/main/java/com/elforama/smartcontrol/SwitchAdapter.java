package com.elforama.smartcontrol;

import android.support.v7.widget.RecyclerView.Adapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.elforama.iotmodel.switches.SwitchInfo;
import java.util.*;

/**
 * Created by jonathanmuller on 9/18/16.
 */
public class SwitchAdapter extends Adapter<SwitchViewHolder> {

    public SwitchListener switchListener;
    private List<SwitchInfo> switches = new ArrayList<>();

    public void loadData(List<SwitchInfo> data) {
        switches = data;
        notifyDataSetChanged();
    }

    @Override
    public SwitchViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_view_switch, parent, false);
        return new SwitchViewHolder(view);
    }

    @Override
    public void onBindViewHolder(SwitchViewHolder holder, int position) {
        holder.load(switches.get(position), switchListener);
    }

    @Override
    public int getItemCount() {
        return switches.size();
    }

    public interface SwitchListener {
        void onSwitchClicked(int id, boolean isOn);
    }
}
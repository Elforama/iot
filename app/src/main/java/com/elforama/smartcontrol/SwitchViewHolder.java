package com.elforama.smartcontrol;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;
import com.elforama.iotmodel.switches.SwitchInfo;

/**
 * Created by jonathanmuller on 10/15/16.
 */
public class SwitchViewHolder extends RecyclerView.ViewHolder {

    private TextView switchName;
    private Switch switchView;

    public SwitchViewHolder(View itemView) {
        super(itemView);
        switchName = (TextView) itemView.findViewById(R.id.switchNameTextView);
        switchView = (Switch) itemView.findViewById(R.id.switch1);
    }

    public void load (final SwitchInfo switchInfo, final SwitchAdapter.SwitchListener switchListener) {
        switchName.setText(switchInfo.name);
        switchView.setChecked(switchInfo.isOn);

        switchView.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (switchListener != null) {
                    switchListener.onSwitchClicked(switchInfo.id, isChecked);
                }
            }
        });
    }
}
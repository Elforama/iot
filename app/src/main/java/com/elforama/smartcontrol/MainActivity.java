package com.elforama.smartcontrol;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;
import com.elforama.iotmodel.switches.SwitchInfo;

import java.util.List;

public class MainActivity extends AppCompatActivity implements MainScreenMvp.View {

    private MainScreenMvp.Presenter presenter;
    private SwitchAdapter adapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        presenter = new MainScreenPresenter();
        adapter = new SwitchAdapter();

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerView);

        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        presenter.onBindView(this);
        presenter.onLoadData();

        adapter.switchListener = new SwitchAdapter.SwitchListener() {
            @Override
            public void onSwitchClicked(int id, boolean isOn) {
                presenter.onSwitchChanged(id, isOn);
            }
        };
    }

    @Override
    public void onDataLoaded(List<SwitchInfo> switchInfoList) {
        adapter.loadData(switchInfoList);
    }

    @Override
    public void onError(String error) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
    }
}
